$(document).ready(function () {

  // mobile menu
	$('.mobile-btn').on('click', function () {
		$('body').addClass('noscroll');
		$('.nav').addClass('active');
    $('.menu-overlay').toggleClass('active');
	});
	$('.close-menu').on('click', function () {
		$('body').removeClass('noscroll');
		$('.nav').removeClass('active');
    $('.menu-overlay').removeClass('active');
	});

  // Подменю
  $('.have-sub').click(function (e) {
    e.preventDefault();
    $(this).closest('body').toggleClass('sub-menu-open');
    $('body').addClass('noscroll');
  });
  $('.menu-overlay').click(function () {
    $('body').removeClass('noscroll');
    $(this).closest('body').removeClass('sub-menu-open');
  });
  $('.close-sub-menu').click(function () {
    $(this).closest('body').removeClass('sub-menu-open');
  });

  // Вход в кабинет (мобильный)
  $('.login-mobile').click(function () {
    $(this).closest('body').toggleClass('cabinet-mobile-open');
    $('body').addClass('noscroll');
  });
  $('.close-cabinet-mobile').click(function () {
    $(this).closest('body').removeClass('cabinet-mobile-open');
  });

  // Background (установка background в html через атрибут "data-bg")
  var bgSelector = $(".bg-img");
  bgSelector.each(function (index, elem) {
    var element = $(elem),
        bgSource = element.data('bg');
    element.css('background-image', 'url(' + bgSource + ')');
  });

  // Выпадающий список
  $('.select').on('click','.placeholder',function(){
    var parent = $(this).closest('.select');
    if ( ! parent.hasClass('is-open')){
    parent.addClass('is-open');
    $('.select.is-open').not(parent).removeClass('is-open');
    }else{
    parent.removeClass('is-open');
    }
  }).on('click','ul>li',function(){
    var parent = $(this).closest('.select');
    parent.removeClass('is-open').find('.placeholder').text( $(this).text() );
    parent.find('input[type=hidden]').attr('value', $(this).attr('data-value') );
  });

  // -- Закрываем селект при клике вне элемента и при скролинге
  $(document).click( function(e){
    if ( $(e.target).closest('.select').length ) {
      return;
    }
    $('.select').removeClass('is-open');
    $('.cabinet-overlay').removeClass('active');
  });
  $(document).scroll(function (e){  
    if ( $(e.target).closest('.select').length ) {
      return;
    }
    $('.select').removeClass('is-open');
  });
  
  $('.common-select').on('click', function(){
    $(this).toggleClass('active');
    $('.cabinet-overlay').toggleClass('active')
    $('.common-select').not(this).removeClass('active');
  })

  $('.cabinet-overlay').click(function () {
    $('.common-select').removeClass('active');
  });

  // sticky header (Только для desctop)
  if ($(window).width() > 1201) {
    $(window).on('scroll', function() {
      if ($(window).scrollTop() > 1000) {
          $('.header').addClass('header-fixed-top');
      } else {
          $('.header').removeClass('header-fixed-top');
      }
    });
  } else {
    $(window).on('scroll', function() {
      if ($(window).scrollTop() > 400) {
          $('.header').addClass('header-fixed-top');
      } else {
          $('.header').removeClass('header-fixed-top');
      }
    });
  }
  
  // Плавная прокрутка
  $(document).ready(function() {
    $('.sm-scroll').click(function(){
    var el = $(this).attr('href');
    el = el.replace(/[^\#]*/, '');
    $('body,html').animate({
    scrollTop: $(el).offset().top}, 1000);
    return false;
    });
  });

  // Testimonial Slider
  $('.testimonials-slider').slick({ 
    centerMode:true,  
    arrows:true,
    dots:false,
    prevArrow: '<span class="prev-slide"><svg viewBox="0 0 9 16" fill="none"><path d="M8 1L1 8L8 15" stroke="currentColor" stroke-linecap="round"/></svg></span>',
    nextArrow: '<span class="next-slide"><svg viewBox="0 0 9 16" fill="none"><path d="M1 1L8 8L1 15" stroke="currentColor" stroke-linecap="round"/></svg></span>',
    slidesToShow: 3,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 769,
        settings: "unslick"
      }
    ]
  });
  
  // Team slider
  $('.team-slider').slick({ 
    infinite:false,  
    arrows:true,
    dots:false,
    appendArrows: '.team-slider-arrows',
    prevArrow: '<span class="prev-slide"><svg viewBox="0 0 9 16" fill="none"><path d="M8 1L1 8L8 15" stroke="currentColor" stroke-linecap="round"/></svg></span>',
    nextArrow: '<span class="next-slide"><svg viewBox="0 0 9 16" fill="none"><path d="M1 1L8 8L1 15" stroke="currentColor" stroke-linecap="round"/></svg></span>',
    slidesToShow: 4,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 769,
        settings: "unslick"
      }
    ]
  });

  // Blog slider
  $('.blog-slider').slick({ 
    infinite:false,  
    arrows:true,
    dots:false,
    appendArrows: '.blog-slider-arrows',
    prevArrow: '<span class="prev-slide"><svg viewBox="0 0 9 16" fill="none"><path d="M8 1L1 8L8 15" stroke="currentColor" stroke-linecap="round"/></svg></span>',
    nextArrow: '<span class="next-slide"><svg viewBox="0 0 9 16" fill="none"><path d="M1 1L8 8L1 15" stroke="currentColor" stroke-linecap="round"/></svg></span>',
    slidesToShow: 2,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 769,
        settings: "unslick"
      }
    ]
  });
 
  // Sertificat slider
  $('.sertificat-slider').slick({ 
    infinite:false,  
    arrows:true,
    dots:false,
    appendArrows: '.sertificat-slider-arrows',
    prevArrow: '<span class="prev-slide"><svg viewBox="0 0 9 16" fill="none"><path d="M8 1L1 8L8 15" stroke="currentColor" stroke-linecap="round"/></svg></span>',
    nextArrow: '<span class="next-slide"><svg viewBox="0 0 9 16" fill="none"><path d="M1 1L8 8L1 15" stroke="currentColor" stroke-linecap="round"/></svg></span>',
    slidesToShow: 1,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 769,
        settings: "unslick"
      }
    ]
  });

  // Popup
  $('.js--popup').on('click', function (e) {
    e.preventDefault();
    let btn = $(this).data('modal');
    $('#' + btn).addClass('active');
    $('.popup-overlay').show();
    $('body').toggleClass('noscroll');
  })

  $('.close-popup, .popup-overlay, .close-popup-btn').on('click', function (e) {
    e.preventDefault();
    $('.popup-overlay').hide();
    $('.popup-window').removeClass('active');
    $('body').removeClass('noscroll');
  })

  // Login
  $('.login-wrapper').hide();
  $('.cabinet').on('click', function(){
    $(this).toggleClass('active');
    $('.login-wrapper').slideToggle(100);
    $(this).closest('body').toggleClass('overlay');
    $('body').toggleClass('noscroll');
  })

  // Open text
  $('.btn--more-text').on('click', function(e){
    e.preventDefault();
    $(this).closest('body').toggleClass('text-open');
    $(this).toggleClass('active');
  })

  // Faq accordeon
  $('.faq__head').click(function () {
    $(this).closest('.faq-acc').toggleClass('active');
    $(this).toggleClass('active').next().slideToggle();
    $('.faq__head').not(this).removeClass('active').next().slideUp();
    $('.faq__head').not(this).closest('faq-acc').removeClass('active');
  });

  // Service accordeon
  $('.service__head').click(function () {
    $(this).closest('.service-acc').toggleClass('active');
    $(this).toggleClass('active').next().slideToggle();
    $('.service__head').not(this).removeClass('active').next().slideUp();
    $('.service__head').not(this).closest('service-acc').removeClass('active');
  });

  // tabs
  $("ul.tabs li").click(function () {
    $(".tab_content").hide();
    var activeTab = $(this).attr("rel");
    $("#" + activeTab).fadeIn();
    $("ul.tabs li").removeClass("active");
    $(this).addClass("active");
  });

  // tabs cont
  $("ul.tabs-cont li").click(function () {
    $(".tab___cont__content").hide();
    var activeTab = $(this).attr("rel");
    $("#" + activeTab).fadeIn();
    $("ul.tabs-cont li").removeClass("active");
    $(this).addClass("active");
  });

  /* Move to Top */
  $(window).scroll(function () {
    if ($(this).scrollTop() > 200) {
      $('.page-up').fadeIn();
    } else {
      $('.page-up').fadeOut();
    }
  });
  $('.page-up').click(function () {
    $('body,html').animate({
      scrollTop: 0
    }, 400);
    return false;
  });

  // Запуск виджетов (Google Map) 
  $(".fancybox").fancybox({
    iframe: {
        preload: false
    }
  });

  // Filter
  $(function() {
    var newSelection = "";
    $(".flavor-nav a").click(function(){
      $("#all-flavors").fadeTo(200, 0.10);
      $(".flavor-nav a").removeClass("current");
      $(this).addClass("current");
      newSelection = $(this).attr("rel");
      $(".flavor").not("."+newSelection).slideUp();
      $("."+newSelection).slideDown();
      $("#all-flavors").fadeTo(600, 1);
    });
  });

  // Скрыть/Показать блоки в мобильной версии
  $('.show-more').click(function (e) {
    e.preventDefault();
    $(this).closest('body').toggleClass('more-blocks');
    $(this).toggleClass('active');
    $('.show-more').not(this).closest('body').removeClass('more-blocks');
  });



})